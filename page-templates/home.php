<?php
/**
 * Template Name: Home
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
    <section class="home__main-banner">
        <div class="container-fluid wrap">
            <div class="row center-xs">

                <div class="col-xs-12 col-md-10">
                 <?php if (get_field('intro_pretitle')): ?>
                        <h4 class="banner__preline font-size__small--x font-weight__normal font-size__medium--x font__secondary"><?php the_field('intro_pretitle'); ?></h4>
                    <?php endif ?>

                    <?php if(get_field('intro_title')): ?>
                       <h1 class="banner__title font-weight__semibold"><?php the_field('intro_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_field('intro_text')): ?>
                       <p class="banner-desc font-size__medium"><?php the_field('intro_text'); ?></p>
                   <?php endif; ?>

                    <div class="url__wrapper display__inline--block">
                        <form action="https://guruho.tel/">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/gh-iso.svg" class="margin-right__big--x"> <span class="text-color__titles">guruho.tel/</span>
                            <input type="text" name="siteurl" placeholder="yourhotelname">
                            <button class="btn btn--primary"><i class="fas fa-angle-right"></i></button>
                        </form>
                    </div>

                   <?php if(get_field('intro_cta_text')): ?>
                        <footer>
                             <a href="<?php the_field('intro_cta_link'); ?>" class="btn btn--primary btn-primary btn--primary padding__medium--x display__inline--block margin-top__mega font-weight__normal">
                                <?php the_field('intro_cta_text'); ?>
                            </a>
                        </footer>
                   <?php endif; ?>

                   <?php if(get_field('intro_image')): ?>
                        <div class="banner__img--wrapper">
                            <div class="circle"></div>
                            <img src="<?php $img = get_field('intro_image'); echo $img['sizes']['large']; ?>" class="banner__img">
                        </div>
                   <?php endif; ?>
                </div>
            </div>

            <?php if(have_rows('intro_features')) : ?>
                <div class="banner__features">
                    <div class="row center-xs">
                        <?php
                            $features_count = 1;
                            while(have_rows('intro_features')): the_row();
                        ?>
                            <div class="col-xs-10 col-sm-4 col-md-3 start-xs <?php if($features_count%2 == 0) echo 'col-md-offset-6 col-sm-offset-2'; ?>">
                                <div class="banner__feature">
                                    <span class="padding__medium--x icon border-radius__medium display__inline--block vertical-align__middle">
                                        <i class="<?php the_sub_field('icon'); ?> font-size__mega"></i>
                                    </span>
                                    <h4 class="margin-bottom__normal margin-top__big"><?php the_sub_field('title'); ?></h5>
                                    <?php the_sub_field('text'); ?>
                                </div>
                            </div>
                        <?php $features_count++; endwhile; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <img src="<?php bloginfo('template_directory'); ?>/assets/images/intro-bg.svg" class="banner__bg">
    </section>

    <section class="home__features background-color__titles padding__section text-color__white">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-12 col-md-6">
                    <?php if(get_field('features_pretitle')): ?>
                        <h4 class="text-color__orange font-size__small--x font-weight__normal font-size__small--x font__secondary letter-spacing__medium"><?php the_field('features_pretitle'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('features_title')): ?>
                        <h2 class="text-color__white"><?php the_field('features_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('features_text')): ?>
                        <?php the_field('features_text'); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row center-xs">
                <div class="col-xs-12 col-md-10">
                    <div class="home__features--list margin-top__mega">
                        <?php $i = 0; if(have_rows('features_features')) : while(have_rows('features_features')): the_row(); ?>
                            <div class="features__feature text-color__white border-radius__normal">
                                <i class="icon margin-right__big--x <?php the_sub_field('icon'); ?>"></i>
                                <h5 class="font-size__medium font-weight__semibold"><?php the_sub_field('title'); ?></h5>
                                <?php the_sub_field('text'); ?>
                            </div>
                        <?php $i++; endwhile; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="home__cta" class="padding__section">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-11 col-md-9">

                    <?php if(get_field('cta_title')): ?>
                        <h2><?php the_field('cta_title'); ?></h2>
                    <?php endif; ?>
                </div>

                <div class="col-xs-11 col-md-8">

                    <?php if(get_field('cta_text')): ?>
                        <?php the_field('cta_text'); ?>
                    <?php endif; ?>

                    <div class="url__wrapper display__inline--block">
                        <form action="https://guruho.tel/">
                            <img src="<?php bloginfo('template_directory'); ?>/src/images/gh-iso.svg" class="margin-right__big--x"> <span class="text-color__titles">guruho.tel/</span>
                            <input type="text" name="siteurl" placeholder="yourhotelname">
                            <button class="btn btn--primary"><i class="fas fa-angle-right"></i></button>
                        </form>
                    </div>

                    <footer>
                        <a href="<?php the_field('cta_cta_link'); ?>" class="btn btn--primary btn-primary btn--primary border-radius__mega--x background-color__main text-transform__uppercase letter-spacing__medium font-weight__medium text-color__white padding__medium--x display__inline--block margin-top__mega banner-button">
                            <?php the_field('cta_cta_text'); ?>
                        </a>

                        <?php if(get_field('cta_cta_secondary_text')): ?>
                            <a href="<?php the_field('cta_cta_secondary_link'); ?>" class="text-color__text btn--text margin-left__mega--x"><?php the_field('cta_cta_secondary_text'); ?></a>
                        <?php endif; ?>
                    </footer>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; get_footer();
