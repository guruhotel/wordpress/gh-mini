<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
	<script type='text/javascript' src='//kit.fontawesome.com/549f1fa78b.js?ver=5.4.2'></script>
    <?php wp_head(); ?>
</head>

<body <?php body_class('animate-in'); ?>>
    <header class="primary-header">
        <div class="container-fluid wrap">
            <div class="row middle-xs">
                <div class="col-xs-6">
                    <a href="<?php bloginfo('wpurl'); ?>">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo.svg">
                    </a>
                </div>
                <div class="col-xs-6">
                    <nav class="main-menu">
                        <?php wp_nav_menu( array( 'theme_location' => 'menu-main', 'container' => '' ) ); ?>
                    </nav>
                </div>
            </div>
        </div>
    </header>
