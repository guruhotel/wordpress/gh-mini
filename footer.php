    <footer class="primary-footer">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-6">
                    <a href="<?php bloginfo('wpurl'); ?>">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo.svg">
                    </a>
                </div>
            </div>
        </div>
    </footer>

<?php wp_footer(); ?>
</body>
</html>
