//Imports
import slick from 'slick-carousel';
import AOS from 'aos';
import lity from 'lity';
import Litepicker from 'litepicker';
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

//Site code
jQuery(document).ready(function($){
    //Animations
    AOS.init();

    //Fade in/out animation
    window.addEventListener("beforeunload", function () {
      document.body.classList.add("animate-out");
    });

    if($('body').hasClass('page-template-home') && $(window).width() > 768){
        var introSection = gsap.timeline({
          scrollTrigger:{
            trigger: ".home__main-banner .banner__img--wrapper img",
            start: "-200",
            end: "+=300",
            width: '355',
            pin: false,
            scrub: 0.2,
          }
        })
        .to('.home__main-banner .banner__img--wrapper img', {
          width: '250',
          duration: 1,
          ease: 'none',
        });

        var circle = gsap.timeline({
          scrollTrigger:{
            trigger: ".home__main-banner .banner__img--wrapper .circle",
            start: "-100",
            end: "+=150",
            width: '78%',
            pin: false,
            scrub: 0.2,
          }
        })
        .to('.home__main-banner .banner__img--wrapper .circle', {
              width: '100%',
              duration: 1,
              ease: 'easeIn',
              paddingBottom: '100%',
              opacity: 0,
              top: '-100',
        });

        var featuresSection = gsap.timeline({
          scrollTrigger:{
            trigger: ".home__features",
            start: "-450",
            end: "+=300",
            backgroundSize: 'auto 85%',
            pin: false,
            scrub: 0.2,
            ease: "power1.out",
          }
        })
        .to('.home__features', {
            backgroundSize: 'auto 100%',
            marginTop: '-200'
        });
    }

    $('.url__wrapper input').keyup(function(){
        if($(this).val() == ''){
            $(this).parents('.url__wrapper').find('.btn').fadeOut('slow');
        }
        else{
            $(this).parents('.url__wrapper').find('.btn').fadeIn('slow');
        }

    });
});
